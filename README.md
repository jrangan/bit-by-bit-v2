BIT BY BIT
-----------
WEBAPP:
localhost:3000

SERVER:
localhost:5000/todo

DOCKER COMMANDS
----------------
START CONTAINER:
docker-compose up

STOP CONTAINER:
docker-compose stop

REMOVE CONTAINER AND NETWORKS
docker-compose down

REMOVE CONTAINERS, NETWORKS AND VOLUMES
docker-compose down --volumes