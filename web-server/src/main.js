const TodoServer = require('./server.js')
const MongoRepository = require('./mongo-todo-repository.js')
const InMemoryRepository = require('./in-memory-todo-repository.js')

if (process.env.DB_REPOSITORY === 'mongo') {
  const mongoTodoRepo = new MongoRepository()
  const todoServer = new TodoServer(mongoTodoRepo)
  todoServer.start()
}

if (process.env.DB_REPOSITORY === 'inMemory') {
  const inMemoryTodoRepo = new InMemoryRepository()
  const todoServer = new TodoServer(inMemoryTodoRepo)
  todoServer.start()
}
