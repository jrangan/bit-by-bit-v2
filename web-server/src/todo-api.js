const Todo = require('./todo')
class TodoApi {
  constructor(todoRepository) {
    this.todoRepository = todoRepository
  }

  error() {
    return { status: 'fail', data: {} }
  }

  success(data) {
    return {
      status: 'success',
      data,
    }
  }

  async createTodo(todoObj) {
    const todo = new Todo(todoObj.name)
    await this.todoRepository.save(todo)
    return this.success(todo)
  }

  async retrieveTodos() {
    return this.success(await this.todoRepository.getAll())
  }

  async retrieveTodoById(id) {
    if (await this.todoRepository.hasTodo(id)) {
      return this.success(await this.todoRepository.getById(id))
    }
    return this.error()
  }

  async removeTodo(id) {
    if (await this.todoRepository.hasTodo(id)) {
      await this.todoRepository.remove(id)
      return this.success({})
    } else {
      return this.error()
    }
  }

  async updateItemName(id, newTodoObj) {
    if (await this.todoRepository.hasTodo(id)) {
      await this.todoRepository.updateName(id, newTodoObj.name)
      return this.success(await this.todoRepository.getById(id))
    } else {
      return this.error()
    }
  }

  async updateItemStatus(id, newTodoObj) {
    if (await this.todoRepository.hasTodo(id)) {
      await this.todoRepository.updateStatus(id, newTodoObj.complete)
      return this.success(await this.todoRepository.getById(id))
    } else {
      return this.error()
    }
  }
}
module.exports = TodoApi
