import PropTypes from "prop-types";
import * as React from "react";
import "./CreateTask.css";

//ADDS TASK TO TASK LIST

const CreateTask = ({ updateTasks }) => {
  const [taskName, setTaskName] = React.useState(""); //Task Name (State)
  //On change of input textboxes, store string value in the state
  const handleOnChangeNameEvent = ({ target: { value } }) => setTaskName(value);

  const handleOnClickEvent = () => {
    //On click pass the name and desc values to the function that adds items to the todoList array
    updateTasks(taskName);

    //Clear input elements
    setTaskName("");
  };
  return (
    <div className="createTaskPanel">
      <input
        name="taskName"
        type="text"
        placeholder="Enter a To-Do..."
        value={taskName}
        onChange={handleOnChangeNameEvent}
      />
      <button onClick={handleOnClickEvent}>+</button>
    </div>
  );
};

CreateTask.propTypes = {
  updateTasks: PropTypes.func,
};

CreateTask.defaultProps = {
  updateTasks: () => {},
};
export default CreateTask;
