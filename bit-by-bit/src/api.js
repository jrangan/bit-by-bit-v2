const apiGetAll = async (url, updateData) => {
  const response = await fetch(url, {
    headers: { "Content-Type": "application/json" },
  });
  const jsonData = await response.json();
  const displayList = jsonData.todoList.map((task) => ({
    id: task.id,
    name: task.name,
    done: task.complete,
  }));
  updateData(displayList);
};

const apiCreateTodo = async (url, todoName) => {
  const response = await fetch(url, {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      name: todoName,
    }),
  });

  const jsonData = await response.json();
  return { id: jsonData.id, name: jsonData.name, done: jsonData.complete };
};

const apiPatch = async (url, id, newValue, todoProperty) => {
  return await fetch(url + "/" + id, {
    method: "PATCH",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({ [todoProperty]: newValue }),
  });
};

const apiDelete = async (url, id) => {
  return await fetch(url + "/" + id, {
    method: "DELETE",
  });
};

export { apiGetAll, apiCreateTodo, apiPatch, apiDelete };
