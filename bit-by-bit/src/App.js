import * as React from "react";
import "./App.css";
import CreateTask from "./CreateTask";
import TaskList from "./TaskList";
import { apiCreateTodo, apiDelete, apiGetAll, apiPatch } from "./api";

const App = () => {
  const [todoList, setTodoList] = React.useState([]);

  const url = "http://localhost:5000/todo";

  React.useEffect(() => {
    apiGetAll(url, setTodoList);
  }, []);

  const addNewItemToTodoList = async (itemName) => {
    //API call Add to DB
    const newItem = await apiCreateTodo(url, itemName);
    console.log("ADDED");
    const updatedList = [...todoList, newItem];
    setTodoList(updatedList);
  };

  const removeItemFromTodoList = async (id) => {
    const updateList = todoList.filter((task) => task.id !== id);
    setTodoList(updateList);

    //API call Remove from DB
    await apiDelete(url, id);
    console.log("REMOVED");
  };

  const updateItemFromTodoList = async (id, updatedName) => {
    const listCopy = todoList.map((task) => {
      if (task.id === id) {
        return { ...task, name: updatedName };
      }
      return { ...task };
    });
    setTodoList(listCopy);

    //API update name of todo in DB
    await apiPatch(url, id, updatedName, "name");
    console.log("UPDATED");
  };
  const strikeItemInTodoList = async (id) => {
    let changedValue;
    const updateList = todoList.map((task) => {
      if (task.id === id) {
        if (task.done === false) {
          changedValue = true;
          return { ...task, done: true };
        } else {
          changedValue = false;
          return { ...task, done: false };
        }
      }
      return { ...task };
    });
    setTodoList(updateList);

    //API update complete of todo in DB
    apiPatch(url, id, changedValue, "complete");
    console.log("COMPLETE");
  };

  return (
    <div className="App">
      <h1>TO-DO LIST</h1>
      <div className="ToDoPanel">
        <CreateTask updateTasks={addNewItemToTodoList} />
        <TaskList
          updateTaskName={updateItemFromTodoList}
          removeTask={removeItemFromTodoList}
          setComplete={strikeItemInTodoList}
          tasks={todoList}
        />
      </div>
    </div>
  );
};

export default App;
