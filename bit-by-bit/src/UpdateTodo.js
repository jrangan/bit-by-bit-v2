import * as React from "react";
//import PropTypes from "prop-types";
const UpdateTodo = ({ updateName }) => {
  const handleOnUpdateNameEvent = ({ target: { value } }) => {
    updateName(value);
  };
  return (
    <div>
      <h2>UPDATE TO-DO NAME</h2>
      <h3>PLEASE ENTER A NEW NAME</h3>
      <input name="updateName" onChange={handleOnUpdateNameEvent} />
    </div>
  );
};

export default UpdateTodo;
