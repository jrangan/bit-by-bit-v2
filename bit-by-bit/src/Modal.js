import * as React from "react";

const Modal = ({ setModalToggle, updateTaskName, taskId }) => {
  const [newNameValue, setNewNameValue] = React.useState("");
  const handleOnUpdateNameEvent = (e) => setNewNameValue(e.target.value);
  const handleUpdateName = () => {
    updateTaskName(taskId, newNameValue);
    setNewNameValue("");
  };
  return (
    <div className="modal">
      <input
        name="modalInput"
        type="text"
        placeholder="Update Task Name..."
        value={newNameValue}
        onChange={handleOnUpdateNameEvent}
      />
      <button
        type="button"
        onClick={() => {
          handleUpdateName();
          setModalToggle(false);
        }}
      >
        SUBMIT
      </button>
      <button type="button" onClick={() => setModalToggle(false)}>
        CLOSE
      </button>
    </div>
  );
};

export default Modal;
