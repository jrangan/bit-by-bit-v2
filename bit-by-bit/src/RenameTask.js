import * as React from "react";
import PropTypes from "prop-types";

// RENAMES A TASK IN THE EXISTING LIST

const RenameTask = () => {
  const [newTaskName, updateNewTaskName] = React.useState(""); // Task Name in List (State)

  //On change of input textboxes, store string value in the state
  const handleOnChangeNewNameEvent = ({ target: { value } }) =>
    updateNewTaskName(value);

  return (
    <div>
      <h3>Rename Task</h3>
      <input
        name="taskName"
        value={newTaskName}
        onChange={handleOnChangeNewNameEvent}
      />
    </div>
  );
};

export default RenameTask;
