import PropTypes from "prop-types";
import * as React from "react";
import Modal from "./Modal";
import "./TaskList.css";

//DISPLAYS LIST OF TASKS

const TaskList = (props) => {
  const { updateTaskName, removeTask, setComplete, tasks = [] } = props;

  const [modalToggle, setModalToggle] = React.useState(false);
  const [currentTaskIdForModal, setCurrentTaskIdForModal] = React.useState(0);
  const handleRemoveTask = (id, removeTask) => {
    removeTask(id);
  };

  const handleStrike = (id, setComplete) => {
    setComplete(id);
  };

  const handleModalToggle = (id) => {
    setCurrentTaskIdForModal(id);
    setModalToggle(true);
  };

  return (
    <React.Fragment>
      <ul>
        {tasks.map((task) => {
          return (
            <div className="listElements" key={task.id}>
              <li key={task.id}>
                <input
                  type="checkbox"
                  checked={task.done}
                  onChange={() => handleStrike(task.id, setComplete)}
                />
                {task.name}
                <button
                  type="button"
                  onClick={() => handleModalToggle(task.id)}
                >
                  E
                </button>
                <button
                  type="button"
                  onClick={() => handleRemoveTask(task.id, removeTask)}
                >
                  X
                </button>
              </li>
            </div>
          );
        })}
      </ul>
      {modalToggle && (
        <div className="modalUpdateName">
          <Modal
            taskId={currentTaskIdForModal}
            setModalToggle={setModalToggle}
            updateTaskName={updateTaskName}
          />
        </div>
      )}
    </React.Fragment>
  );
};

TaskList.propTypes = {
  updatedName: PropTypes.string,
  updatedTaskName: PropTypes.func,
  removeTask: PropTypes.func,
  setComplete: PropTypes.func,
  tasks: PropTypes.array,
};

TaskList.defaultProps = {
  updatedName: "",
  updatedTaskName: () => {},
  removeTask: () => {},
  setComplete: () => {},
  tasks: [],
};

export default TaskList;
